﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryFunction
{
	public interface IQueryFunction
	{
		QueryResult Query(SearchTreeNode node);
	}
}
