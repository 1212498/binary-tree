﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryFunction
{
	public class QueryResult
	{
		private Object _value;

		public QueryResult(Object obj)
		{
			this._value = obj;
		}

		public object Value
		{
			get
			{
				return _value;
			}
		}
	}
}
