﻿using BinaryTree.QueryFunction;
using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinaryTree.Traversal;

namespace BinaryTree.BinarySearchTree
{
	public class BinarySearchTree : SearchTree
	{
		private InternalNode _root;

		public void Add(int key)
		{
			InternalNode q, p;
			q = p = _root;
			while (p != null)
			{
				q = p;
				if (key == p.Key)
				{
					return;
				}
				else if (key < p.Key)
				{
					p = p.Left;
				}
				else
				{
					p = p.Right;
				}
			}
			if (p == null)
			{
				p = new InternalNode(key);
				if (_root == null)
				{
					_root = p;
				}
				else if (key < q.Key)
				{
					q.Left = p;
				}
				else
				{
					q.Right = p;
				}
			}
		}

		public void AddRange(List<int> keys)
		{
			foreach (int k in keys)
			{
				this.Add(k);
			}
		}

		public SearchTreeNode GetRoot()
		{
			return BinarySearchTreeNode.GetNode(_root);
		}

		public virtual QueryResult Query(IQueryFunction function)
		{
			return function.Query(BinarySearchTreeNode.GetNode(_root));
		}

		public void Remove(int key)
		{
			InternalNode t, q, p;
			q = p = _root;
			while (p != null)
			{
				if (p.Key == key)
				{
					break;
				}
				q = p;
				if (key < p.Key)
				{
					p = p.Left;
				}
				else
				{
					p = p.Right;
				}
			}
			if (p == null)
			{
				return;
			}
			if (p.Left != null && p.Right != null)
			{
				t = p;
				q = p;
				p = p.Left;
				while (p.Right != null)
				{
					q = p;
					p = p.Right;
				}
				t.Key = p.Key;
			}
			if (p.Left != null)
			{
				t = p.Left;
			}
			else
			{
				t = p.Right;
			}
			if (p.Key == _root.Key)
			{
				_root = t;
			}
			else if (q.Left != null && q.Left.Key == p.Key)
			{
				q.Left = t;
			}
			else if (q.Right != null)
			{
				q.Right = t;
			}
		}

		public virtual SearchResult Search(ISearchFunction function)
		{
			return function.Search(this.GetRoot());
		}

		public List<SearchTreeNode> Travel(Traversal.Traversal traversal)
		{
			return traversal.Travel(this.GetRoot());
		}
	}
}
