﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchFunction
{
	public class SearchMaxValue : ISearchFunction
	{
		public SearchResult Search(SearchTreeNode node)
		{
			if (node == null)
			{
				return null;
			}
			var i = node;
			SearchTreeNode result = null;
			while (i != null)
			{
				result = i;
				i = i.Right;
			}
			return new SearchResult(result);
		}
	}
}
