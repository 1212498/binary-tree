﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchFunction
{
	public class SearchKey : ISearchFunction
	{
		private int _key;

		public SearchKey(int key)
		{
			this._key = key;
		}

		public SearchResult Search(SearchTreeNode node)
		{
			if (node == null)
			{
				return null;
			}
			var i = node;
			while (i != null)
			{
				if (i.Value == this._key)
				{
					return new SearchResult(node);
				}
				else if (_key < i.Value)
				{
					i = i.Left;
				}
				else
				{
					i = i.Right;
				}
			}
			return null;
		}
	}
}
