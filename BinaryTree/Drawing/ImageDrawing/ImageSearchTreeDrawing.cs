﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.Drawing.ImageDrawing
{
	public class ImageSearchTreeDrawing : SearchTreeDrawing
	{
		public ImageSearchTreeDrawing(SearchTree tree) : base(tree)
		{

		}

		public override Object Draw()
		{
			ImageSearchTreeNodeDrawing nd = new ImageSearchTreeNodeDrawing(this.Tree.GetRoot());
			int center = 0;
			return nd.Draw(out center);
		}

	}
}
