﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.Drawing
{
	public abstract class SearchTreeDrawing
	{
		private SearchTree _tree;

		public SearchTreeDrawing(SearchTree tree)
		{
			this._tree = tree;
		}

		protected SearchTree Tree
		{
			get
			{
				return _tree;
			}

		}

		public abstract Object Draw();
	}
}
