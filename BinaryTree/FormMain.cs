﻿using BinaryTree.Drawing.ImageDrawing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinaryTree
{
	public partial class FormMain : Form
	{
		public FormMain()
		{
			InitializeComponent();
		}

		private void FormMain_Load(object sender, EventArgs e)
		{

			SearchTree tree = new BinarySearchTree.BinarySearchTree();
			tree.AddRange(new List<int>() { 8, 4, 12, 2, 6, 10, 14, 1, 3, 5, 7, 11, 13 });
			ImageSearchTreeDrawing d = new ImageSearchTreeDrawing(tree);
			Image i = (Image)d.Draw();
			pictureBox1.Image = i;
		}
	}
}
