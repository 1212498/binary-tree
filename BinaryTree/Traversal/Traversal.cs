﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.Traversal
{
	public abstract class Traversal
	{
		public abstract List<SearchTreeNode> Travel(SearchTreeNode node);

		protected void ConcatenateList(List<SearchTreeNode> desc, List<SearchTreeNode> src)
		{
			foreach (SearchTreeNode n in src)
			{
				desc.Add(n);
			}
		}
	}
}
